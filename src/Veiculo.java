abstract class Veiculo {
    protected int vehicleType;
    protected String fuelType;
    protected int velMed;
    protected int cargaMax;
    protected String id;

    abstract double rendimento(double peso);

    public abstract String getId();
}