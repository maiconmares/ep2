public class Carreta extends Veiculo{
    private String id;

    public Carreta(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    double rendimento(double peso) {
        double rendimentoRealCarreta;
        rendimentoRealCarreta = 8 - 0.0002 * peso;
        return rendimentoRealCarreta;
    }
}