import javax.swing.*;

//Retirar algumas desses atributos e adicioná-los como variáveis dos métodos mais pertinentes, pois nem tudo pode ser considerado atributos
public class Calcular {
    private double peso;
    private double distancia;
    private double maxTime;
    private static final double alcool = 3.499;
    private static final double gasolina = 4.449;
    private static final double diesel = 3.869;
    private double lucro;

    public Calcular(double dist, double peso, double horas, double lucro) {
        this.distancia = dist;
        this.peso = peso;
        this.maxTime = horas;
        this.lucro = lucro;
    }

    private double custoOperacaoVan(double peso, double distancia, double lucro) {
        double custoVan;
        double preco;
        Van van = new Van("0");

        custoVan = ((distancia / (van.rendimento(peso))) * diesel);
        preco = custoVan / (1 - lucro);

        return preco;
    }

    private double custoOperacaoCarreta(double peso, double distancia, double lucro) {
        double custoCarreta;
        double preco;
        Carreta carreta = new Carreta("0");

        custoCarreta = ((distancia / (carreta.rendimento(peso))) * diesel);
        preco = custoCarreta / (1 - lucro);

        return preco;
    }

    private double[] custoOperacaoMoto(double peso, double distancia, double lucro) {
        double precoMotoAlcool, precoMotoGasolina;
        double custoMotoAlcool;
        double custoMotoGasolina;
        double[] precosMoto;
        precosMoto = new double[2];
        Moto moto = new Moto("0");

        custoMotoAlcool = ((distancia / (moto.rendimentoAlcool(peso))) * alcool);
        custoMotoGasolina = ((distancia / (moto.rendimento(peso))) * gasolina);

        precoMotoAlcool = custoMotoAlcool / (1 - lucro);
        precoMotoGasolina = custoMotoGasolina / (1 - lucro);

        precosMoto[0] = precoMotoAlcool;
        precosMoto[1] = precoMotoGasolina;


        return precosMoto;
    }

    private double[] custoOperacaoCarro(double peso, double distancia, double lucro) {
        double custoCarroAlcool, custoCarroGasolina;
        double precoCarroGasolina, precoCarroAlcool;
        double[] precosCarro;
        precosCarro = new double[2];
        Carro carro = new Carro("0");

        custoCarroAlcool = ((distancia / (carro.rendimentoAlcool(peso))) * alcool);
        custoCarroGasolina = ((distancia / (carro.rendimento(peso))) * gasolina);

        precoCarroAlcool = custoCarroAlcool / (1 - lucro);
        precoCarroGasolina = custoCarroGasolina / (1 - lucro);

        precosCarro[0] = precoCarroAlcool;
        precosCarro[1] = precoCarroGasolina;

        return precosCarro;
    }

    public int[] viabilidade(double distancia, double peso, double maxTime, double lucro) {
        int[] veicDisp; //Array que indica os veiculos disponiveis.
        int[] disponCustoBen;
        int menorCustoPos, menorCustoBenPos;
        veicDisp = new int[4];
        disponCustoBen = new int[6];
        double[] custosVeics;
        custosVeics = new double[4];
        double[] capturaCustos, capturaCustosCarro, capturaCustosMoto, custoBenCarro, custoBenMoto, comparaCustos;
        capturaCustos = new double[2];
        capturaCustosCarro = new double[2];
        capturaCustosMoto = new double[2];
        custoBenCarro = new double[2];
        custoBenMoto = new double[2];
        comparaCustos = new double[6];

        double menorCustoTemp, capturaCustosVan, capturaCustosCarreta, custoBenCarreta, custoBenVan, melhorCustoBenValor;

        //Primeiro critério: calcula se o veículo consegue transportar a quantidade de carga desejada
        //Carreta
        if (peso > 30000) veicDisp[0] = 0;
        else veicDisp[0] = 1;
        //Van
        if (peso > 3500) veicDisp[1] = 0;
        else veicDisp[1] = 1;
        //Carro
        if (peso > 360) veicDisp[2] = 0;
        else veicDisp[2] = 1;
        //Moto
        if (peso > 50) veicDisp[3] = 0;
        else veicDisp[3] = 1;

        //Segundo criterio: calcula se a velocidade do veículo é suficiente para atender a entrega
        //Carreta
        if (((distancia / 60) > maxTime) || veicDisp[0] == 0) veicDisp[0] = 0;
        else veicDisp[0] = 1;
        //Van
        if (((distancia / 80) > maxTime) || veicDisp[1] == 0) veicDisp[1] = 0;
        else veicDisp[1] = 1;
        //Carro
        if (((distancia / 100) > maxTime) || veicDisp[2] == 0) veicDisp[2] = 0;
        else veicDisp[2] = 1;
        //Moto
        if (((distancia / 110) > maxTime) || veicDisp[3] == 0) veicDisp[3] = 0;
        else veicDisp[3] = 1;

        //Retorna os veículos que atendem a entrega
        if (veicDisp[0] == 0 && veicDisp[1] == 0 && veicDisp[2] == 0 && veicDisp[3] == 0) {
            JOptionPane.showMessageDialog(null, "Nenhum veículo pode atender a entrega"); //Nenhum veículo pode atender a entrega
        }

        return veicDisp;
    }

    public String[] calculaVeicMaisRap(double distancia, double peso, double maxTime, double lucro) {
        double capturaCustosVan, capturaCustosCarreta;
        double[] capturaCustosCarro, capturaCustosMoto;
        int[] veicDisp;
        String[] capturaCustosAux;
        capturaCustosCarro = new double[2];
        capturaCustosMoto = new double[2];
        veicDisp = new int[4];
        capturaCustosAux = new String[4];
        veicDisp = viabilidade(distancia, peso, maxTime, lucro);

        //Variáveis para o veículo mais rápido
        capturaCustosCarro = custoOperacaoCarro(peso, distancia, lucro);
        capturaCustosMoto = custoOperacaoMoto(peso, distancia, lucro);
        capturaCustosCarreta = custoOperacaoCarreta(peso, distancia, lucro);
        capturaCustosVan = custoOperacaoVan(peso, distancia, lucro);

        //Retorna veículo mais rápido
        if (veicDisp[3] != 0) {
            System.out.println("O veículo mais rápido para a entrega é a Moto.");
            System.out.println("Você pode escolher:");
            System.out.printf("1-Moto R$ %.2f(álcool) ou 2-Moto R$ %.2f(gasolina)\n", capturaCustosMoto[0], capturaCustosMoto[1]);
            capturaCustosAux[0] = String.valueOf(capturaCustosMoto[0]);
            capturaCustosAux[1] = String.valueOf(capturaCustosMoto[1]);
            capturaCustosAux[2] = "1";
            capturaCustosAux[3] = "Moto";
            return capturaCustosAux;
        } else if (veicDisp[2] != 0 && veicDisp[3] == 0) {
            System.out.println("O veículo mais rápido para a entrega é o Carro.");
            System.out.println("Você pode escolher:");
            System.out.printf("1-Carro R$ %.2f(álcool) ou 2-Carro R$ %.2f(gasolina)\n", capturaCustosCarro[0], capturaCustosCarro[1]);
            capturaCustosAux[0] = String.valueOf(capturaCustosCarro[0]);
            capturaCustosAux[1] = String.valueOf(capturaCustosCarro[1]);
            capturaCustosAux[2] = "1";
            capturaCustosAux[3] = "Carro";
            return capturaCustosAux;
        } else if (veicDisp[1] != 0 && veicDisp[2] == 0 && veicDisp[3] == 0) {
            System.out.println("O veículo mais rápido para a entrega é a Van.");
            System.out.printf("1-Van R$ %.2f(Diesel)\n", capturaCustosVan);
            capturaCustosAux[0] = String.valueOf(capturaCustosVan);
            capturaCustosAux[2] = "0"; //Indica que é desnecessária a escolha de um combustível, pois só existe um para este veículo
            capturaCustosAux[3] = "Van";
            return capturaCustosAux;
        } else if (veicDisp[0] != 0 && veicDisp[1] == 0 && veicDisp[2] == 0 && veicDisp[3] == 0) {
            System.out.println("O veículo mais rápido para a entrega é a Carreta.");
            System.out.printf("1-Carreta R$ %.2f(Diesel)\n", capturaCustosCarreta);
            capturaCustosAux[0] = String.valueOf(capturaCustosCarreta);
            capturaCustosAux[2] = "0"; //Indica que é desnecessária a escolha de um combustível, pois só existe um para este veículo
            capturaCustosAux[3] = "Carreta";
            return capturaCustosAux;
        }
        return null;

    }

    public String[] calculaVeicMaiorBen(double distancia, double peso, double maxTime, double lucro) {
        double melhorCustoBenValor, menorCustoBenPos, capturaCustosCarreta, capturaCustosVan;
        double[] comparaCustos, capturaCustosCarro, capturaCustosMoto, disponCustoBen;
        int[] veicDisp;
        String[] informVeicBen;
        informVeicBen = new String[2];

        veicDisp = new int[4];
        comparaCustos = new double[6];
        capturaCustosCarro = new double[2];
        capturaCustosMoto = new double[2];
        disponCustoBen = new double[6];

        veicDisp = viabilidade(distancia, peso, maxTime, lucro);

        //Variáveis para o veículo com melhor custo benefício
        capturaCustosCarro = custoOperacaoCarro(peso, distancia, lucro);
        capturaCustosMoto = custoOperacaoMoto(peso, distancia, lucro);
        capturaCustosCarreta = custoOperacaoCarreta(peso, distancia, lucro);
        capturaCustosVan = custoOperacaoVan(peso, distancia, lucro);

        comparaCustos[0] = capturaCustosCarreta / maxTime;
        comparaCustos[1] = capturaCustosVan / maxTime;
        comparaCustos[2] = capturaCustosCarro[0] / maxTime; //Carro com álcool
        comparaCustos[3] = capturaCustosCarro[1] / maxTime; //Carro com gasolina
        comparaCustos[4] = capturaCustosMoto[0] / maxTime; //Moto com álcool
        comparaCustos[5] = capturaCustosMoto[1] / maxTime; //Moto com gasolina

        disponCustoBen[0] = veicDisp[0]; //Viabilidade de Carreta
        disponCustoBen[1] = veicDisp[1]; //Viabilidade de Van
        disponCustoBen[2] = veicDisp[2]; //Viabilidade de Carro com álcool
        disponCustoBen[3] = veicDisp[2]; //Viabilidade de Carro com gasolina
        disponCustoBen[4] = veicDisp[3]; //Viabilidade de Moto com álcool
        disponCustoBen[5] = veicDisp[3]; //Viabilidade de Moto com gasolina


        //Retorna o veículo com menor custo benefício
        melhorCustoBenValor = 999999999;
        menorCustoBenPos = 0;
        for (int i = 0; i < 6; i++) {
            if ((melhorCustoBenValor > comparaCustos[i]) && disponCustoBen[i] != 0) {
                melhorCustoBenValor = comparaCustos[i];
                menorCustoBenPos = i;
            }
        }

        //Mostrar combustíveis que geram este custo benefício
        if (menorCustoBenPos == 0) {
            System.out.println("O veículo com maior custo benefício é a Carreta.");
            System.out.printf("2-Carreta R$ %.2f por hora\n", melhorCustoBenValor);
            informVeicBen[0] = String.valueOf(melhorCustoBenValor * (distancia / 60));
            informVeicBen[1] = "Carreta";
            return informVeicBen;
        } else if (menorCustoBenPos == 1) {
            System.out.println("O veículo com maior custo benefício é a Van.");
            System.out.printf("2-Van R$ %.2f por hora\n", melhorCustoBenValor);
            informVeicBen[0] = String.valueOf(melhorCustoBenValor * (distancia / 80));
            informVeicBen[1] = "Van";
            return informVeicBen;
        } else if (menorCustoBenPos == 2 || menorCustoBenPos == 3) {
            System.out.println("O veículo com maior custo benefício é o Carro.");
            System.out.printf("2-Carro R$ %.2f por hora\n", melhorCustoBenValor);
            informVeicBen[0] = String.valueOf(melhorCustoBenValor * (distancia / 100));
            informVeicBen[1] = "Carro";
            return informVeicBen;
        } else if (menorCustoBenPos == 4 || menorCustoBenPos == 5) {
            System.out.println("O veículo com maior custo benefício é a Moto.");
            System.out.printf("2-Moto R$ %.2f por hora\n", melhorCustoBenValor);
            informVeicBen[0] = String.valueOf(melhorCustoBenValor * (distancia / 110));
            informVeicBen[1] = "Moto";
            return informVeicBen;
        }

        return null;
    }

    public String[] calculaVeicMaisEcon(double distancia, double peso, double maxTime, double lucro) {
        int[] veicDisp;
        double[] custosVeics, capturaCustos;
        double menorCustoTemp, menorCustoPos;
        String[] inforVeicEcon;
        inforVeicEcon = new String[2];
        custosVeics = new double[4];
        capturaCustos = new double[2];
        veicDisp = new int[4];
        veicDisp = viabilidade(distancia, peso, maxTime, lucro);

        //Avalia o veículo com menor custo para a operação
        //Carreta
        if (veicDisp[0] != 0) {
            custosVeics[0] = custoOperacaoCarreta(peso, distancia, lucro);
        } else {
            custosVeics[0] = 0; //Indica que não atende o requisito
        } //Van
        if (veicDisp[1] != 0) {
            custosVeics[1] = custoOperacaoVan(peso, distancia, lucro);
        } else {
            custosVeics[1] = 0;
        } //Carro
        if (veicDisp[2] != 0) {
            capturaCustos = custoOperacaoCarro(peso, distancia, lucro);
            menorCustoTemp = capturaCustos[0]; //Carro com álcool
            if (menorCustoTemp > capturaCustos[1]) menorCustoTemp = capturaCustos[1];
            custosVeics[2] = menorCustoTemp;
        } else {
            custosVeics[2] = 0;
        } //Moto
        if (veicDisp[3] != 0) {
            capturaCustos = custoOperacaoMoto(peso, distancia, lucro);
            menorCustoTemp = capturaCustos[0]; //Moto com álcool
            if (menorCustoTemp > capturaCustos[1]) menorCustoTemp = capturaCustos[1];
            custosVeics[3] = menorCustoTemp;
        } else {
            custosVeics[3] = 0;
        }

        menorCustoTemp = 999999;
        menorCustoPos = 0;
        for (int i = 0; i < 4; i++) {
            if (custosVeics[i] != 0) {
                if (menorCustoTemp > custosVeics[i]) {
                    menorCustoTemp = custosVeics[i];
                    menorCustoPos = i;
                }
            }
        }

        //Retorna veículo com menor custo para a operação
        if (menorCustoPos == 0) {
            System.out.println("O veículo com menor custo para a operação é a Carreta.");
            System.out.printf("3-Carreta R$ %.2f\n", menorCustoTemp);
            inforVeicEcon[0] = String.valueOf(menorCustoTemp);
            inforVeicEcon[1] = "Carreta";
            return inforVeicEcon;
        } else if (menorCustoPos == 1) {
            System.out.println("O veículo com menor custo para a operação é a Van.");
            System.out.printf("3-Van R$ %.2f\n", menorCustoTemp);
            inforVeicEcon[0] = String.valueOf(menorCustoTemp);
            inforVeicEcon[1] = "Van";
            return inforVeicEcon;
        } else if (menorCustoPos == 2) {
            System.out.println("O veículo com menor custo para a operação é o Carro.");
            System.out.printf("3-Carro R$ %.2f\n", menorCustoTemp);
            inforVeicEcon[0] = String.valueOf(menorCustoTemp);
            inforVeicEcon[1] = "Carro";
            return inforVeicEcon;
        } else if (menorCustoPos == 3) {
            System.out.println("O veículo com menor custo para a operação é a Moto.");
            System.out.printf("3-Moto R$ %.2f\n", menorCustoTemp);
            inforVeicEcon[0] = String.valueOf(menorCustoTemp);
            inforVeicEcon[1] = "Moto";
            return inforVeicEcon;
        }
        return null;
    }


}