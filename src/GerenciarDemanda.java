import javax.print.attribute.standard.JobPriority;
import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GerenciarDemanda {
    private List<Moto> motos;
    private List<Carro> carros;
    private List<Carreta> carretas;
    private List<Van> vans;
    private List demandas;
    private Scanner ler;

    public GerenciarDemanda() {
        //Talvez criar a lista novamente toda vez que o construtor é chamado seja errado
        //instanciaVeiculos();
        //novaDemanda();
    }

    public int novaDemanda(double distancia, double peso, double lucro, double tempo) {
        double saldoAtual;
        int escolherVeic, escolherComb, qtdDemandas;
        int[] veicDisp;
        String[] valorVeicRap, valorVeicBen, valorVeicEcon;
        String veicEscolhido = "";
        veicDisp = new int[4];
        valorVeicRap = new String[4];
        valorVeicBen = new String[2];
        valorVeicEcon = new String[2];
        saldoAtual = 0;

        JOptionPane.showMessageDialog(null, "Nova demanda iniciada!");

        GerenciarArquivos demanda = new GerenciarArquivos();

        if (demanda.verificaZeroVeic()) {
            JOptionPane.showMessageDialog(null, "Não existe nenhum veículo cadastrado!. Cadastre veículos para continuar.");
            return 0;
        } else if (demanda.verificaTodosVeicOcupados()) {
            JOptionPane.showMessageDialog(null, "Todos os veículos estão ocupados. Registre mais veículos ou libere os veículos ocupados para continuar.");
            return 0;
        }


        Calcular novoCalculo = new Calcular(distancia, peso, tempo, lucro);
        //valorVeicRap = novoCalculo.calculaVeicMaisRap();
        //valorVeicBen = novoCalculo.calculaVeicMaiorBen();
        //valorVeicEcon = novoCalculo.calculaVeicMaisEcon();

        tela_escolher_veiculo veicDispon = new tela_escolher_veiculo(distancia, peso, tempo, lucro);


        veicDisp = novoCalculo.viabilidade(distancia, peso, tempo, lucro);



        demanda.resumoDemandas();
        return 0;
    }

    public void instanciaVeiculos(int opcaoVeic, int qtdVeic) {
        int quantidade, qtdIds;
        String idMoto, idCarro, idCarreta, idVan;
        motos = new ArrayList<>();
        carros = new ArrayList<>();
        carretas = new ArrayList<>();
        vans = new ArrayList<>();

        /*while (true) {*/

        GerenciarArquivos arquivoLer = new GerenciarArquivos();
        qtdIds = arquivoLer.lerQtdIds(opcaoVeic);
        System.out.printf("A quantidade do veículo x registrado é %d\n", qtdIds);


        //switch (opcaoVeic) {
        //  case 1:
        if (opcaoVeic == 1) {
            for (int i = qtdIds + 1; i < qtdVeic + qtdIds + 1; i++) {
                idMoto = "10" + i;
                try {
                    arquivoLer.registrarVeic(idMoto, "Moto-");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Moto moto = new Moto(idMoto);
                motos.add(moto);
            }

            JOptionPane.showMessageDialog(null, qtdVeic + "motos foram registradas");
        }
        //  break;
        //case 2:
        if (opcaoVeic == 2) {
            for (int i = qtdIds + 1; i < qtdVeic + qtdIds + 1; i++) {
                idCarro = "20" + i;
                try {
                    arquivoLer.registrarVeic(idCarro, "Carro-");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Carro carro = new Carro(idCarro);
                carros.add(carro);
            }
            JOptionPane.showMessageDialog(null, qtdVeic + "carros foram registrados");
        }
        //  break;
        //case 3:
        if (opcaoVeic == 3) {
            for (int i = qtdIds + 1; i < qtdVeic + qtdIds + 1; i++) {
                idCarreta = "30" + i;
                try {
                    arquivoLer.registrarVeic(idCarreta, "Carreta-");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Carreta carreta = new Carreta(idCarreta);
                carretas.add(carreta);
            }
            JOptionPane.showMessageDialog(null, qtdVeic + "carretas foram registradas");
        }
        //break;
        //case 4:
        if (opcaoVeic == 4) {
            for (int i = qtdIds + 1; i < qtdVeic + qtdIds + 1; i++) {
                idVan = "40" + i;
                try {
                    arquivoLer.registrarVeic(idVan, "Van-");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Van van = new Van(idVan);
                vans.add(van);
            }
            JOptionPane.showMessageDialog(null, qtdVeic + "vans foram registradas");
        }
        //break;
        //default:
        //  System.out.println("Esta opção não existe. Escolha novamente!");
        /*}*/


            /*
            System.out.println("Motos armazenadas:");
            for (int j = 0; j < motos.size(); j ++){
                Moto saidaM =  motos.get(j);
                System.out.println(saidaM.getId());
            }

            System.out.println("Carros armazenados:");
            for (int j = 0; j < carros.size(); j ++){
                Carro saidaC = carros.get(j);
                System.out.println(saidaC.getId());
            }

            System.out.println("Carretas armazenadas:");
            for (int j = 0; j < carretas.size(); j ++){
                Carreta saidaCt = carretas.get(j);
                System.out.println(saidaCt.getId());
            }

            System.out.println("Vans armazenadas:");
            for (int j = 0; j < vans.size(); j ++){
                Van saidaV = vans.get(j);
                System.out.println(saidaV.getId());
            }*/

        //}
    }


}