public class Carro extends Veiculo{
    private String id;

    public Carro(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //O rendimento padrão é gasolina
    double rendimento(double peso) {
        double rendimentoRealCarroGasolina;
        rendimentoRealCarroGasolina = 14 - 0.025 * peso;
        return rendimentoRealCarroGasolina;
    }

    double rendimentoAlcool(double peso) {
        double rendimentoRealCarroAlcool;
        rendimentoRealCarroAlcool = 12 - 0.0231 * peso;
        return rendimentoRealCarroAlcool;
    }
}

