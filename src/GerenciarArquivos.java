import javax.swing.*;
import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Scanner;

public class GerenciarArquivos {
    private File arquivo;
    private FileReader input;
    private BufferedReader armazenaTemp;
    private FileWriter writer;
    private BufferedWriter escreveTemp;

    public GerenciarArquivos() {
    }

    public int lerQtdIds(int tipoVeiculo) {
        int qtdIdsRegistrados = 0;
        String idVeiculoProcurado = "";
        String linha = "";
        String dividir[] = new String[3];

        try {
            if (tipoVeiculo == 1) idVeiculoProcurado = "Moto";
            if (tipoVeiculo == 2) idVeiculoProcurado = "Carro";
            if (tipoVeiculo == 3) idVeiculoProcurado = "Carreta";
            if (tipoVeiculo == 4) idVeiculoProcurado = "Van";
        } catch (Exception e) {
            System.out.println("Tipo de veículo inexistente!");
        }

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");
                if (dividir[0].equals(idVeiculoProcurado)) {
                    qtdIdsRegistrados++;
                }

                linha = armazenaTemp.readLine();
            }
            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }
        return qtdIdsRegistrados;
    }

    public boolean verificaZeroVeic() {
        String linha = "";

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            if (linha == null) {
                return true;
            }
            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }
        return false;
    }

    public boolean verificaTodosVeicOcupados() {
        int veiculosOcupados = 0, qtdDemandas, qtdVeicRegistra = 0;
        String linha = "";
        String[] dividir;
        dividir = new String[3];

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                if (linha != null) qtdVeicRegistra++;
                dividir = linha.split("-");
                if (dividir[2].equals("true")) {
                    veiculosOcupados++;
                }

                linha = armazenaTemp.readLine();
            }
            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        if (qtdVeicRegistra == veiculosOcupados) {
            return true;
        }

        return false;
    }

    public void registrarVeic(String idVeic, String tipoVeic) throws IOException {
        String linha = "\n" + tipoVeic + idVeic + "-false";
        System.out.println(linha);
        String caminho = "src/arquivo.txt";
        if (verificaZeroVeic()) {
            linha = tipoVeic + idVeic + "-false";
        }
        writer = new FileWriter(caminho, true);
        escreveTemp = new BufferedWriter(writer);

        escreveTemp.append(linha);

        escreveTemp.close();
        writer.close();

    }

    public void registrarDemanda(double valorDemanda, int numeroDemanda) {
        String linha, caminho;
        linha = "demanda-" + numeroDemanda + "-" + valorDemanda;
        try {
            caminho = "src/demandas.txt";
            writer = new FileWriter(caminho, true);
            escreveTemp = new BufferedWriter(writer);

            escreveTemp.append(linha + "\n");

            escreveTemp.close();
            writer.close();

        } catch (Exception e) {
            System.out.println("Erro ao abrir o arquivo!");
        }
    }

    public int lerQtdDemandas() {
        int qtdDemandas = 0;
        String linha;
        String dividir[];
        dividir = new String[3];

        try {
            arquivo = new File("src/", "demandas.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");

                if (dividir[0].equals("demanda")) {
                    qtdDemandas++;
                }

                linha = armazenaTemp.readLine();
            }
            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }
        return qtdDemandas;
    }

    public void resumoDemandas() {
        String linha;
        String dividir[];
        double total = 0;
        dividir = new String[3];

        try {
            arquivo = new File("src/", "demandas.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");

                System.out.println(dividir[0] + " " + dividir[1] + " - " + "R$ " + dividir[2]);
                total += Double.parseDouble(dividir[2]);
                linha = armazenaTemp.readLine();
            }
            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        System.out.printf("TOTAL: R$ %.2f\n", total);
    }

    public void veiculoIniFrete(String tipoVeic) {
        String linha = "", linha2 = "", linhaAlterar = "", linhaAlterada = "";
        String[] dividir;
        dividir = new String[3];

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");
                if (dividir[0].equals(tipoVeic) && dividir[2].equals("false")) { //False significa que o veículo não está ocupado
                    linhaAlterar = dividir[0] + "-" + dividir[1] + "-" + dividir[2];
                    linhaAlterada = dividir[0] + "-" + dividir[1] + "-" + "true";
                    break;
                }

                linha = armazenaTemp.readLine();
            }

            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        try {
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha2 = armazenaTemp.readLine();
            ArrayList<String> salvar = new ArrayList();
            while (linha2 != null) {
                if (!linha2.equals(linhaAlterar)) {
                    salvar.add(linha2);
                }

                linha2 = armazenaTemp.readLine();
            }

            armazenaTemp.close();

            FileWriter novoArquivo = new FileWriter(arquivo);
            escreveTemp = new BufferedWriter(novoArquivo);
            for (int i = 0; i < salvar.size(); i++) {
                escreveTemp.write(salvar.get(i));
                escreveTemp.newLine();
            }
            escreveTemp.write(linhaAlterada);

            escreveTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");

        }

        //arquivo.delete();
        //novoArquivo.renameTo(arquivo);
    }

    public boolean verificaVeicDispon(String veicDispon) {
        String linha = "", captura = "";
        String[] dividir;
        dividir = new String[3];

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");
                if (dividir[0].equals(veicDispon) && dividir[2].equals("false")) { //False significa que o veículo não está ocupado
                    captura = "ok";
                    break;
                }

                linha = armazenaTemp.readLine();
            }

            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        if (!captura.equals("ok")) {
            JOptionPane.showMessageDialog(null,"Não há nenhum veículo disponível! Cadastre outro veículo ou libere um veículo ocupado!");
            return false;
        }

        return true;
    }

    public int liberarVeic() {
        String linha = "", linha2 = "", linhaAlterar = "", linhaAlterada = "", tipoVeic = "", captura="";
        String[] dividir;
        int option;
        Scanner ler = new Scanner(System.in);
        dividir = new String[3];

        if (verificaZeroVeic()) {
            System.out.println("Não há veículos para liberar, pois a lista está vazia!");
            return 0;
        }

        System.out.println("Digite o número referente ao tipo de veículo que deseja liberar:");
        System.out.println("1-Carreta.");
        System.out.println("2-Van.");
        System.out.println("3-Carro.");
        System.out.println("4-Moto.");
        option = ler.nextInt();

        if (option == 1) {
            tipoVeic = "Carreta";
        } else if (option == 2) {
            tipoVeic = "Van";
        } else if (option == 3) {
            tipoVeic = "Carro";
        } else if (option == 4) {
            tipoVeic = "Moto";
        }

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");
                if (dividir[0].equals(tipoVeic) && dividir[2].equals("true")) { //True significa que o veículo está ocupado
                    captura = "ok";
                    break;
                }

                linha = armazenaTemp.readLine();
            }

            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        if (!captura.equals("ok")) {
            System.out.println("Não há nenhum veículo deste tipo ocupado!");
            return 0;
        }

        try {
            arquivo = new File("src/", "arquivo.txt");
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha = armazenaTemp.readLine();
            while (linha != null) {
                dividir = linha.split("-");
                if (dividir[0].equals(tipoVeic) && dividir[2].equals("true")) { //False significa que o veículo não está ocupado
                    linhaAlterar = dividir[0] + "-" + dividir[1] + "-" + dividir[2];
                    linhaAlterada = dividir[0] + "-" + dividir[1] + "-" + "false";
                    break;
                }

                linha = armazenaTemp.readLine();
            }

            armazenaTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");
        }

        try {
            input = new FileReader(arquivo);
            armazenaTemp = new BufferedReader(input);
            linha2 = armazenaTemp.readLine();
            ArrayList<String> salvar = new ArrayList();
            while (linha2 != null) {
                if (!linha2.equals(linhaAlterar)) {
                    salvar.add(linha2);
                }

                linha2 = armazenaTemp.readLine();
            }

            armazenaTemp.close();

            FileWriter novoArquivo = new FileWriter(arquivo);
            escreveTemp = new BufferedWriter(novoArquivo);
            for (int i = 0; i < salvar.size(); i++) {
                escreveTemp.write(salvar.get(i));
                escreveTemp.newLine();
            }
            escreveTemp.write(linhaAlterada);

            escreveTemp.close();
        } catch (Exception e) {
            System.out.println("Erro ao ler o arquivo!");

        }
        return 0;
    }
}
