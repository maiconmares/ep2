public class Van extends Veiculo{
    private String id;

    public Van(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    double rendimento(double peso) {
        double rendimentoRealVan;
        rendimentoRealVan = 10 - 0.001 * peso;
        return rendimentoRealVan;
    }
}