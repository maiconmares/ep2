public class Moto extends Veiculo{
    private String id;

    public Moto(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    double rendimento(double peso) {
        double rendimentoRealMotoGasolina;
        rendimentoRealMotoGasolina = 50 - 0.3 * peso;
        return rendimentoRealMotoGasolina;
    }

    double rendimentoAlcool(double peso) {
        double rendimentoRealMotoAlcool;
        rendimentoRealMotoAlcool = 43 - 0.4 * peso;
        return rendimentoRealMotoAlcool;
    }
}